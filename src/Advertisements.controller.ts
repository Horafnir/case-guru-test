import { Controller, Get, Query } from '@nestjs/common';
import { IAdvertisement, advertisement } from './core';
import { AdvertisementsService } from './Advertisements.service';
import { ApiQuery } from '@nestjs/swagger';

@Controller('advertisements')
export class AdvertisementsController {
  constructor(private readonly advertisementsService: AdvertisementsService) { }


  @Get('all')  
  async findAll(@Query('page') page: number = 1,
    @Query('sort') sort: 'price' | 'createdAt' = 'price',
    @Query('direction') direction: 'asc' | 'desc' = 'asc',) {
    return this.advertisementsService.findAll(page, sort, direction);
  }

  @Get('byId')
  async findOne(@Query('id') id: number, 
  @Query('fields') fields: string[] = ['price', 'mainPhoto'],) {
    return this.advertisementsService.findOne(id, fields);
  }

  @Get('add')
  @ApiQuery({name: 'title'})
  @ApiQuery({ name: 'description' })
  @ApiQuery({ name: 'images' })
  @ApiQuery({ name: 'price' })

  async create(@Query() req:advertisement): Promise<IAdvertisement> {
    return this.advertisementsService.create(req);
  }
}
