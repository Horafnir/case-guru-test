import { Module } from '@nestjs/common';
import { AdvertisementsController } from './Advertisements.controller';
import { AdvertisementsService } from './Advertisements.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AdvertisementModel } from './mysql';

@Module({
  imports: [TypeOrmModule.forRoot({
    type: 'mysql',
    host: 'db',
    port: 3306,
    username: 'test',
    password: 'test',
    database: 'test',
    entities: [AdvertisementModel],
    synchronize: true,
  }), TypeOrmModule.forFeature([AdvertisementModel])],
  controllers: [AdvertisementsController],
  providers: [AdvertisementsService],
})
export class AppModule {}
