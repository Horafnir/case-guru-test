import { Injectable, HttpException, HttpStatus, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { AdvertisementModel } from './mysql';
import { IAdvertisement } from './core';

@Injectable()
export class AdvertisementsService {
  constructor(
    @InjectRepository(AdvertisementModel)
    private advertisementsRepository: Repository<AdvertisementModel>,
  ) { }
  private readonly logger = new Logger(AdvertisementsService.name);

  async findAll(page: number = 1, sort: 'price' | 'createdAt' = 'price', direction: 'asc' | 'desc' = 'asc') {
    this.logger.debug('AdvertisementsService', 'findAll')
    const [results, total] = await this.advertisementsRepository.findAndCount({
      take: 10,
      skip: 10 * (page - 1),
      order: {
        [sort]: direction.toUpperCase(),
      },
      select: ['id', 'title', 'images', 'price'],
    });

    if (total === 0) {
      throw new HttpException('No records found', HttpStatus.NOT_FOUND);
    }

    return results.map(ad => ({
      title: ad.title,
      mainPhoto: ad.mainPhoto,
      price: ad.price,
    }));
  }

  async findOne(id: number, fields: string[] = ['title', 'price', 'mainPhoto']): Promise<AdvertisementModel> {
    this.logger.debug('AdvertisementsService', 'findOne')
    const result = await this.advertisementsRepository.findOneBy({id});

    if (!result) {
      throw new HttpException('No record found', HttpStatus.NOT_FOUND);
    }

    const response: Partial<AdvertisementModel> = {
      title: result.title,
      mainPhoto: result.mainPhoto,
      price: result.price,
    };

    if (fields) {
      if (fields.includes('description')) {
        response.description = result.description;
      }
      if (fields.includes('images')) {
        response.images = response.images;
      }
    }

    return result;
  }

  async create(createAdDto: IAdvertisement): Promise<AdvertisementModel> {
    this.logger.debug('AdvertisementsService', 'create')
    const ad = new AdvertisementModel();
    ad.title = createAdDto.title;
    ad.price = createAdDto.price;
    ad.images = createAdDto.images;
    ad.description = createAdDto.description;

    try {
      return await this.advertisementsRepository.save(ad);
    } catch (e) {
      throw new HttpException('Error creating ad', HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
