import { IsString, MaxLength, IsArray, ArrayMaxSize, IsNumber } from 'class-validator';


export interface IAdvertisement {
  title: string;
  description: string;
  images: string[];
  price: number;
}
export class advertisement implements IAdvertisement{
  @IsString()
  @MaxLength(200)
  title: string;

  @IsString()
  @MaxLength(1000)
  description: string;

  @IsArray()
  @ArrayMaxSize(3)
  images: string[];
  
  @IsNumber()
  price: number;
}