import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common'
import { AppModule } from './Advertisements.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common'

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { logger: ['error', 'warn', 'debug', 'log', 'verbose'] });
  const logger = new Logger('EntryPoint');
  app.useGlobalPipes(new ValidationPipe);
  const config = new DocumentBuilder()
    .setTitle('crypto-rank-test')
    .setDescription('Api Docs for crypto-rank-test')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);
  await app.listen(3000);
  logger.log(`Server running on http://localhost:3000`);
}
bootstrap();
