import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class AdvertisementModel {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 200 })
  title: string;

  @Column('text')
  description: string;

  @Column('simple-array')
  images: string[];

  @Column('decimal')
  price: number;

  get mainPhoto(): string {
    return this.images[0];
  }
}